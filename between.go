package toolbox

import "math/rand"

func BetweenInt(random *rand.Rand, high, low int) int {
	if high < low {
		high, low = low, high
	}
	return random.Intn(high-low+1) + low
}

func BetweenInt32(random *rand.Rand, high, low int32) int32 {
	if high < low {
		high, low = low, high
	}
	return random.Int31n(high-low+1) + low
}

func BetweenInt64(random *rand.Rand, high, low int64) int64 {
	if high < low {
		high, low = low, high
	}
	return random.Int63n(high-low+1) + low
}
