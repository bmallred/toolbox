package toolbox

import (
	"image"
	"math"
)

func EuclideanDistancePoint(a, b image.Point) int {
	dx := a.X - b.X
	dy := a.Y - b.Y
	ds := (dx * dx) + (dy * dy)
	return int(math.Sqrt(math.Abs(float64(ds))))
}

func EuclideanDistanceInt(ax, ay, bx, by int) int {
	dx := ax - bx
	dy := ay - by
	ds := (dx * dx) + (dy * dy)
	return int(math.Sqrt(math.Abs(float64(ds))))
}

func EuclideanDistanceInt32(ax, ay, bx, by int32) int32 {
	dx := ax - bx
	dy := ay - by
	ds := (dx * dx) + (dy * dy)
	return int32(math.Sqrt(math.Abs(float64(ds))))
}

func EuclideanDistanceInt64(ax, ay, bx, by int64) int64 {
	dx := ax - bx
	dy := ay - by
	ds := (dx * dx) + (dy * dy)
	return int64(math.Sqrt(math.Abs(float64(ds))))
}

func EuclideanDistanceFloat32(ax, ay, bx, by float32) float32 {
	dx := ax - bx
	dy := ay - by
	ds := (dx * dx) + (dy * dy)
	return float32(math.Sqrt(math.Abs(float64(ds))))
}

func EuclideanDistanceFloat64(ax, ay, bx, by float64) float64 {
	dx := ax - bx
	dy := ay - by
	ds := (dx * dx) + (dy * dy)
	return math.Sqrt(math.Abs(ds))
}
