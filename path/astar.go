package toolbox

import "container/heap"

// A*
// Derived from https://github.com/beefstack/go-astar

type Paver interface {
	Neighbors() []Paver
	Cost(to Paver) float64
	EstimatedCost(to Paver) float64
}

type node struct {
	paver  Paver
	cost   float64
	rank   float64
	parent *node
	open   bool
	closed bool
	index  int
}

type nodeMap map[Paver]*node

func (nm nodeMap) get(p Paver) *node {
	n, ok := nm[p]
	if !ok {
		n = &node{paver: p}
		nm[p] = n
	}
	return n
}

type priorityQueue []*node

func (q priorityQueue) Len() int {
	return len(q)
}

func (q priorityQueue) Less(i, j int) bool {
	return q[i].rank < q[j].rank
}

func (q priorityQueue) Swap(i, j int) {
	q[i], q[j] = q[j], q[i]
	q[i].index = i
	q[j].index = j
}

func (q *priorityQueue) Push(x interface{}) {
	n := len(*q)
	no := x.(*node)
	no.index = n
	*q = append(*q, no)
}

func (q *priorityQueue) Pop() interface{} {
	old := *q
	n := len(old)
	no := old[n-1]
	no.index = -1
	*q = old[0 : n-1]
	return no
}

func Path(from, to Paver) ([]Paver, float64, bool) {
	nm := nodeMap{}
	nq := &priorityQueue{}
	heap.Init(nq)
	fromNode := nm.get(from)
	fromNode.open = true
	heap.Push(nq, fromNode)

	for {
		if nq.Len() == 0 {
			// There is no path, return found false
			return []Paver{}, 0, false
		}

		current := heap.Pop(nq).(*node)
		current.open = false
		current.closed = true

		if current == nm.get(to) {
			// Found a path to the goal
			p := []Paver{}
			curr := current
			for curr != nil {
				p = append(p, curr.paver)
				curr = curr.parent
			}
			return p, current.cost, true
		}

		for _, neighbor := range current.paver.Neighbors() {
			cost := current.cost + current.paver.Cost(neighbor)
			neighborNode := nm.get(neighbor)
			if cost < neighborNode.cost {
				if neighborNode.open {
					heap.Remove(nq, neighborNode.index)
				}
				neighborNode.open = false
				neighborNode.closed = false
			}

			if !neighborNode.open && !neighborNode.closed {
				neighborNode.cost = cost
				neighborNode.open = true
				neighborNode.rank = cost + neighbor.EstimatedCost(to)
				neighborNode.parent = current
				heap.Push(nq, neighborNode)
			}
		}
	}
}
