package toolbox

import "image"

func RectangleCenter(a image.Rectangle) image.Point {
	x := (a.Min.X + a.Max.X) / 2
	y := (a.Min.Y + a.Max.Y) / 2
	return image.Point{X: x, Y: y}
}

func RectangleCenterInt(a image.Rectangle) (int, int) {
	x := (a.Min.X + a.Max.X) / 2
	y := (a.Min.Y + a.Max.Y) / 2
	return x, y
}

func RectangleCenterInt32(a image.Rectangle) (int32, int32) {
	x := int32(a.Min.X+a.Max.X) / 2
	y := int32(a.Min.Y+a.Max.Y) / 2
	return x, y
}

func RectangleCenterInt64(a image.Rectangle) (int64, int64) {
	x := int64(a.Min.X+a.Max.X) / 2
	y := int64(a.Min.Y+a.Max.Y) / 2
	return x, y
}

func RectangleCenterFloat32(a image.Rectangle) (float32, float32) {
	x := float32(a.Min.X+a.Max.X) / 2.0
	y := float32(a.Min.Y+a.Max.Y) / 2.0
	return x, y
}

func RectangleCenterFloat64(a image.Rectangle) (float64, float64) {
	x := float64(a.Min.X+a.Max.X) / 2.0
	y := float64(a.Min.Y+a.Max.Y) / 2.0
	return x, y
}

func RectangleFromCenter(center image.Point, width, height int) image.Rectangle {
	w := width / 2
	h := height / 2
	return image.Rect(center.X-w, center.Y-h, center.X+w, center.Y+h)
}

func RectangleWithBoundaries(rect image.Rectangle, width, height int) image.Rectangle {
	minx := rect.Min.X
	miny := rect.Min.Y
	maxx := rect.Max.X
	maxy := rect.Max.Y
	if minx < 0 && maxx > width {
		minx = 0
		maxx = width
	} else if miny < 0 && maxy > height {
		miny = 0
		maxy = height
	}

	if minx < 0 {
		maxx = maxx + (-1 * minx)
		minx = 0
	} else if maxx > width {
		minx = minx - (maxx - width)
		maxx = width
	}

	if miny < 0 {
		maxy = maxy + (-1 * miny)
		miny = 0
	} else if maxy > height {
		miny = miny - (maxy - height)
		maxy = height
	}

	return image.Rect(minx, miny, maxx, maxy)
}
