package toolbox

import (
	"image"
)

func CollisionPoint(a, b image.Point) bool {
	return a.X == b.X && a.Y == b.Y
}

func CollisionRect(a, b image.Rectangle) bool {
	return a.Overlaps(b) || b.Overlaps(a)
}

func CollisionCircleInt(a, b image.Point, width, radius int) bool {
	distance := EuclideanDistanceInt(a.X, a.Y, b.X, b.Y)
	return distance <= width/radius
}

func CollisionCircleInt32(a, b image.Point, width, radius int32) bool {
	distance := EuclideanDistanceInt32(int32(a.X), int32(a.Y), int32(b.X), int32(b.Y))
	return distance <= width/radius
}

func CollisionCircleInt64(a, b image.Point, width, radius int64) bool {
	distance := EuclideanDistanceInt64(int64(a.X), int64(a.Y), int64(b.X), int64(b.Y))
	return distance <= width/radius
}

func CollisionCircleFloat32(a, b image.Point, width, radius float32) bool {
	distance := EuclideanDistanceFloat32(float32(a.X), float32(a.Y), float32(b.X), float32(b.Y))
	return distance <= width/radius
}

func CollisionCircleFloat64(a, b image.Point, width, radius float64) bool {
	distance := EuclideanDistanceFloat64(float64(a.X), float64(a.Y), float64(b.X), float64(b.Y))
	return distance <= width/radius
}
