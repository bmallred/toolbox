package toolbox

func ClampInt(x, min, max int) int {
	if x < min {
		return min
	}

	if x > max {
		return max
	}

	return x
}

func ClampInt32(x, min, max int32) int32 {
	if x < min {
		return min
	}

	if x > max {
		return max
	}

	return x
}

func ClampInt64(x, min, max int64) int64 {
	if x < min {
		return min
	}

	if x > max {
		return max
	}

	return x
}

func ClampFloat32(x, min, max float32) float32 {
	if x < min {
		return min
	}

	if x > max {
		return max
	}

	return x
}

func ClampFloat64(x, min, max float64) float64 {
	if x < min {
		return min
	}

	if x > max {
		return max
	}

	return x
}
